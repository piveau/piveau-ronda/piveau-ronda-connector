# Connector

## Functionality
The datastore can process real-time streaming data as well as static datasets. 
It uses a Vertx Cluster to run Connector Verticles to fetch real-time data sources and a Webserver Verticle for users to upload datasets.
The incoming real-time data gets forwarded through Apache Kafka, proccessed with Spark Streaming and saved to an HDFS Cluster. 
The static datasets can be uploaded to HDFS directly. An Http and Websocket Server is running also in Vertx to provide the processed data.


## Prerequisites
* Docker (Production & Development)
* Docker-compose (Production & Development)
* Maven (Development)
* Java (Development)

## Build Instructions
To build and start the project execute the following command in a shell from the project folder.

1. Start Proprietary Projects by executing: "docker-compose up"
2. Build the project by executing: "mvn package"
3. Start the Connector by executing: "mvn exec:java"

Under http://localhost:8080 you find the current description of the DataStore API


## Usage

The Connector is used to connect the results from Spark to the end user of the system.
It offers an HTTP and websocket API to download historic and Real-time data.
It is written in Vertx. The *MainVerticle* is used to start the register and start the different handler.
There are three handler in the *handler* subfolder of the system:
ApiKeyHandler, DatasetHandler and WebsocketHandler.

###ApiKeyHandler
The ApiKeyHandler is a middleware which can be registered to check for a valid API key.
###WebsocketHandler
The WebsocketHandler forwards data from Kafka to Endusers in Real-time.
End users can subscribe on OpenData topics. and get all messages created for that topic.

````java
public class WebsocketHandler implements Closeable {

  private static final Logger LOGGER = Logger.getLogger(WebsocketHandler.class);

  Vertx vertx;
  JsonObject config;
  KafkaConsumer<String, String> kafkaConsumer;
  AtomicReference<Map<String, List<ServerWebSocket>>> topicWebsocketMap;
  Long findNewTopicsJobId;

  /**
   * Handler for WebSocket Streaming of incoming DataStream data
   * @param vertx The Vertx Instance
   * @param config The Vertx Config
   */
  public WebsocketHandler(Vertx vertx, JsonObject config) {
    this.vertx = vertx;
    this.config = config;

    int updateTopicIntervalInSeconds = config.getInteger(Constants.STREAMING_UPDATE_TOPICLIST_SECONDS);


    kafkaConsumer = KafkaConsumer.create(this.vertx, ConfigRetrieverOptionsProvider.kafkaConsumerConfig(config));
    topicWebsocketMap = new AtomicReference<>(new HashMap<>());

    // Start Periodic task to look for new Open Data Topics
    findNewTopicsJobId = vertx.setPeriodic(1000 * updateTopicIntervalInSeconds, id -> updateTopicMap());
    
    // Handle Kafka Message
    kafkaConsumer
      .subscribe(Pattern.compile(KafkaTopics.OPENDATA_PREFIX + ".*"))
      .handler(this::kafkaMessageToWebsockets);
  }
}
````

In the constructor method the vertx instance and config is used as parameters.
The constructor creates a *KafkaConsumer*.
It also creates an empty map where all topics are going to be registered as keys.
The values of this map are lists of open websockets consuming from that topic.
The periodically started method looks for new OpenData topics and registers them in the map.
The created consumer then subscribes on all *OpenData* Topics and executes the *kafkaMessageToWebsockets*
method on every message.

####UpdateTopicMap
The UpdateTopicMap method is responsible for registering new topics in the *topicWebsocketMap*. It does this by
creating a list of all topics and checking their presence in the map.
*filterOpenDataTopics* and *createTopicHashMap* are helpermethods to create the topic list.
```java
public class WebsocketHandler implements Closeable {
    
    private void updateTopicMap(){
        LOGGER.debug("Looking for new Topics");
        Promise<Map<String, List<PartitionInfo>>> newTopics = Promise.promise();
        kafkaConsumer.listTopics(newTopics);
        newTopics
          .future()
          .map(Map::keySet)
          .compose(this::filterOpenDataTopics)
          .compose(this::createTopicHashMap)
          .setHandler(topicsFuture -> {
            if (topicsFuture.succeeded()) {
              Map<String, List<ServerWebSocket>> newTopicMap = topicsFuture.result();
    
              // Update Values
              newTopicMap
                .keySet()
                .parallelStream()
                .filter(key -> !topicWebsocketMap.get().containsKey(key))
                .forEach(key -> {
                  LOGGER.info("Found new Topic: " + key);
                  topicWebsocketMap.get().put(key, newTopicMap.get(key));
                });
            }
          });
      }

}
```

####KafkaMessageToWebsockets

The KafkaMessageToWebosckets method. Looks for the topic on every received KafkaMessage and forwards it to registered websockets in the map.
It does this by simply looking up the topic in the map and forward the method to each stream.
Websockets subscribed to the *root* stream receive all messages from all topics.
````java
public class WebsocketHandler implements Closeable {
     private void kafkaMessageToWebsockets(KafkaConsumerRecord<String, String> record){
         LOGGER.debug("Received Message from Topic:" + record.topic());
         // Send Message To Topic Websockets
         topicWebsocketMap.get()
           .getOrDefault(record.topic(), new LinkedList<>())
           .parallelStream()
           .forEach(ws -> ws.writeTextMessage(record.value()));
     
         // Send Message to Root Websockets
         topicWebsocketMap.get().getOrDefault("", new LinkedList<>())
           .parallelStream()
           .forEach(ws -> ws.writeTextMessage(record.value()));
       }
}
````
###Handle Websocket registration
Websockets can be registered with the *handleWebsocket* method. This method checks the *path*
the weboscket is registered and parses the topic out of the path. In case the topic does not exist a message with
existing topics is sent and the weboscket is closed afterwards.
For existing topics the websocket is simply added to the list for that topic in the *topicWebsocketMap*.
*CloseHandler* and *DrainHandler* take care of removing the not used websocket from the list again.
````java
public class WebsocketHandler implements Closeable {
    public void handleWebsocket(ServerWebSocket ws) {
        String path = ws.path();
        // Reject Wrong Path
        if (!path.startsWith("/datastream")) {
          ws.reject();
          return;
        }
        // Parse Topic Name
        String[] topic = ws.path().split("/datastream/");
        String subscribedTopic = topic.length > 1 ? topic[1] : "";
        // Check if Topic actually exists
        if (!topicWebsocketMap.get().containsKey(subscribedTopic)) { // Topic does not exist
          ws.accept();
          String validTopics = topicWebsocketMap.get().keySet().stream()
            .map(t -> "'" + "/datastream/" + t + "'").collect(Collectors.joining(", "));
          ws.writeTextMessage("Stream does not exist. Try one of the following streams: "
              + validTopics,
            voidAsyncResult -> ws.close((short) 404));
        } else { // Topic exists
          topicWebsocketMap.get().get(subscribedTopic).add(ws);
          ws.closeHandler(aVoid -> topicWebsocketMap.get().get(subscribedTopic).remove(ws));
          ws.drainHandler(aVoid -> topicWebsocketMap.get().get(subscribedTopic).remove(ws));
        }
      }
}
````
###DatasetHandler
The DatasetHandler is used for CRUD functionality of static datasets and downloading historic data from data streams in CSV and JSON.
In the constructor method. The *HdfsUtils* and *DatasetService* are intialized and
as well a periodicJob for deleting unused downloaded and uploaded files is started.
The HdfsUtils is a wrapper for HDFS functionality in vertx. It is located in thie *util* folder of the project.
The Dataset is an vertx Service, which provides functionalities for exploring datasets on HDFS.

````java
public class DatasetHandler implements Closeable {
  private static final Logger LOGGER = Logger.getLogger(DatasetHandler.class);

  Vertx vertx;
  JsonObject config;

  DataStoreUtils dataStoreUtils;
  HdfsUtils hdfsUtils;
  DatasetService datasetService;

  CircuitBreaker breaker;
  Path uploadDirectory;
  Path openDataDirectory;
  Tika tika;

  Long periodicDeleteJobId;

  /**
   * HTTP Handler for Dataset and Windowed Datastream Requests
   *
   * @param config The Vertx Config provided by vertx ConfigRetriever
   * @param vertx  The Vertx Instance
   */
  public DatasetHandler(Vertx vertx, JsonObject config) {
    // Instantiate Class Objects
    this.vertx = vertx;
    this.config = config;
    this.dataStoreUtils = new DataStoreUtils(vertx, config);
    this.breaker = CircuitBreaker.create(this.getClass().getCanonicalName(), vertx,
      new CircuitBreakerOptions()
        .setMaxFailures(config.getInteger(Constants.CIRCUITBREAKER_MAXFAILURES))
        .setTimeout(config.getInteger(Constants.CIRCUITBREAKER_TIMEOUT))
        .setResetTimeout(config.getInteger(Constants.CIRCUITBREAKER_RESETTIMEOUT))
    );
    this.tika = new Tika();


    // Init HDFS Util
    breaker.<HdfsUtils>execute(future -> {
      Promise<HdfsUtils> servicePromise = Promise.promise();
      this.hdfsUtils = new HdfsUtils(vertx, config, servicePromise);
      servicePromise.future().onSuccess(future::complete);
      servicePromise.future().onFailure(future::fail);
    });

    // Init Dataset Service
    breaker.<DatasetService>execute(future -> {
      Promise<DatasetService> servicePromise = Promise.promise();
      DatasetService.create(config, vertx, servicePromise);
      servicePromise.future().onSuccess(future::complete);
      servicePromise.future().onFailure(future::fail);
    }).onSuccess(datasetService1 -> this.datasetService = datasetService1);


    // Periodically delete idle temporary files
    int periodicMinutes = this.config.getInteger(Constants.FILESYSTEM_TEMPFILES_DELETEOLDERTHAN) * 60 * 1000;
    periodicDeleteJobId = vertx.setPeriodic(periodicMinutes, id -> dataStoreUtils.deleteTempFiles());

  }
}
````

#### CRUD static Dataset
Functionality for Create, Read, Update and Deleting static datasets each have their own method in the *DatasetHandler* class.
Additionally a method for exploring all uploaded datasets is given.
The *getDatasetList* method is first parsing the *pagination* parameters: *page* and *pageSize*.
Afterwards these parameters are put in the *datasetSerivce* method *getDatasetLocations*. The result is a *DatasetMessage*,
which is specified in the *model* folder of the project.
As the locations of the datastreams are relative , the *setLocationPrefix* method adds
the absolut part to the locations. Afterwards the result is forwarded to the user.
````java
public class DatasetHandler implements Closeable {
  /**
     * HTTP Method to return a List of relative Locations to uploaded Datasets
     *
     * @param context The HTTP RoutingContext for the response
     */
    public void getDatasetList(RoutingContext context) {
      // Parse Arguments
      String path = HttpUtils.normalizedPath(context.normalisedPath());
      int page = 1; // Default Value
      try {
        page = Integer.parseInt(context.queryParam("p").get(0));
      } catch (Exception ignored) {
        LOGGER.debug("No Page given for this Request. Using default Value.");
      }
  
      int pageSize = config.getInteger(Constants.DEFAULT_PAGESIZE); // Default Value
      try {
        int parsedPageSize = Integer.parseInt(context.queryParam("pageSize").get(0));
        pageSize = Math.min(parsedPageSize, config.getInteger(Constants.MAX_PAGESIZE));
      } catch (Exception ignored) {
        LOGGER.debug("No PageSize given for this Request. Using default Value.");
      }
  
      Promise<DatasetMessage> jsonPromise = Promise.promise();
      this.datasetService.getDatasetLocations(page, pageSize, jsonPromise);
      jsonPromise
        .future()
        .map(datasetMessage -> datasetMessage.setLocationPrefix(path + "/"))
        .onSuccess(datasetMessage -> {
          context.response()
            .setStatusCode(datasetMessage.statusCode)
            .end(datasetMessage.toJson().encode());
        })
        .onFailure(error -> HttpUtils.internalServerError(context));
    }
}
````
The getDataset methods streams a Dataset file to the user.
It uses the *pathParam* to get the datasetId for the wanted dataset.
Afterwards it requests metaData for this particular datasetId from the *datasetService*.
In case the dataset exists, filename, fileLength and mimeType are created and put in the response.
Afterwards the *pumpFile* method is used to stream the file from HDFS to the enduser.

````java
public class DatasetHandler implements Closeable {
    /**
       * HTTP Method to return a DatasetFile
       *
       * @param context The HTTP RoutingContext for the response
       */
      public void getDataset(RoutingContext context) {
        // Parse Arguments
        String datasetId = context.pathParam("datasetId");
    
        Promise<DatasetMessage> metaDataPromise = Promise.promise();
        this.datasetService.getDatasetMetadata(datasetId, metaDataPromise);
    
        metaDataPromise
          .future()
          .flatMap(datasetMessage -> {
            Promise<DatasetMessage> result = Promise.promise();
            if (datasetMessage.statusCode == 200) {
              String fileName = datasetMessage.fileNames.get(0);
              String fileLength = datasetMessage.fileSizes.get(0).toString();
              String mimeType = tika.detect(fileName);
    
              context.response()
                .setChunked(true)
                .putHeader("content-type", mimeType)
                .putHeader("content-length", fileLength)
                .putHeader("Content-Disposition", "attachment; filename=" + fileName);
    
              this.hdfsUtils.pumpFile(new Path(datasetId, fileName), context.response())
                .onSuccess(aVoid -> result.complete(datasetMessage))
                .onFailure(result::fail);
            } else {
              result.complete(datasetMessage);
            }
    
            return result.future();
          })
          .onSuccess(datasetLocation -> {
            if (datasetLocation.statusCode == 200) {
              context
                .response()
                .setStatusCode(datasetLocation.statusCode)
                .end();
            } else {
              context
                .response()
                .setStatusCode(datasetLocation.statusCode)
                .end(datasetLocation.toJson().encode());
            }
          })
          .onFailure(error -> HttpUtils.internalServerError(context));
      }
  
}
````
The *getDatastream* method functions in a similar way. As the result for *getDatastream* can have multiple files for download
due to the chunked storage on HDFS, the *pumpFiles* method is used to stream more than one file tho the user one after an other.
