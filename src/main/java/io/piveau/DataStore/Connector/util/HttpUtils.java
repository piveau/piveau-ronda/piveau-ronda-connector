package io.piveau.DataStore.Connector.util;

import io.piveau.DataStore.Connector.model.httpMessage.HttpMessage;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.ArrayUtils;

public class HttpUtils {

  /**
   * HTTP Method to return a Statuscode 500 to the RoutingContext
   *
   * @param context The HTTP RoutingContext for the response
   */
  public static void internalServerError(RoutingContext context) {
    HttpMessage result = new HttpMessage()
      .statusCode(500)
      .message("Internal Server Error");

    context.response().reset();

    context.response()
      .setStatusCode(result.statusCode)
      .end(result.toJson().encode());
  }

  /**
   * Concat to Http Path together
   * @param prefix left part of the Http Path
   * @param suffix right part of the Http Path
   * @return
   */
  public static String concatPath(String prefix, String suffix) {
    return String.join("/",
      ArrayUtils.addAll(prefix.split("/"), suffix.split("/")));
  }

  /**
   * Normalizes an Http Path by deleting an ending '/' character.
   * Example: /path/to/route/ -> /path/to/route
   * @param path The Http Path
   * @return the normalized Http Path
   */
  public static String normalizedPath(String path) {
    return path.replaceFirst("/$", "");
  }
}
