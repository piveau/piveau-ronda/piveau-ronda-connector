package io.piveau.DataStore.Connector.util;

import io.piveau.DataStore.Commons.schema.DataFormat;
import io.piveau.DataStore.Commons.schema.HdfsStructure;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.Pump;
import io.vertx.core.streams.WriteStream;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.StreamUtils;
import org.wisdom.framework.vertx.AsyncInputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class HdfsUtils implements Closeable {
  public static final PathFilter noSparkInternalFiles = path -> !path.toString().contains("/_spark_metadata");
  private static final Logger LOGGER = Logger.getLogger(HdfsUtils.class);
  private final Vertx vertx;
  private final JsonObject config;
  private final Long hdfsHealthCheckMs;
  private final Long connectionTimeTreshholdMs;
  public HdfsStructure hdfsStructure;
  private FileSystem hdfsFileSystem;
  private Long hdfsHealthCheckJobId;

  /**
   * Utils for the HDFS Filesystem in Vertx
   *
   * @param vertx        the Vertx instance
   * @param config       the Vertx config
   * @param readyHandler Handler that returns the created instance when it is ready
   */
  public HdfsUtils(Vertx vertx, JsonObject config, Handler<AsyncResult<HdfsUtils>> readyHandler) {
    this.vertx = vertx;
    this.config = config;
    this.hdfsStructure = new HdfsStructure(config.getString(Constants.HDFS_ROOT_DIRECTORY));
    this.connectionTimeTreshholdMs = config.getLong(Constants.HDFS_CLIENT_CONNECTION_THRESHOLD_MS, 5000L);
    this.hdfsHealthCheckMs = config.getLong(Constants.HDFS_CLIENT_HEALTHCHECK_MS, 60000L);


    // Instantiate connection to HDFS Filesystem
    openHDFSConnection();
    vertx.setPeriodic(hdfsHealthCheckMs, aLong -> hdfsClientHealthCheck());
    readyHandler.handle(Future.succeededFuture(this));
  }

  /**
   * Checks if a Path exists on the hdfs FileSystem
   *
   * @param remotePath Path to check
   * @return Future of true if path exists False otherwise
   */
  public Future<Boolean> pathExists(Path remotePath) {
    Promise<Boolean> result = Promise.promise();
    vertx.executeBlocking(promise -> {
      try {
        promise.complete(this.hdfsFileSystem.exists(remotePath));
      } catch (IOException e) {
        String message = "Could not check if Path exists";
        promise.fail(message);
        LOGGER.error(message);
      }
    }, result);

    return result.future();
  }

  /**
   * List all Files and Folder of a given Path
   *
   * @param remotePath the path
   * @param recursive  if set to true also List all Files of all Subfolders recursively
   * @return List of FileStatuses for all Files and Folder included in the Path
   */
  public Future<List<FileStatus>> listFiles(Path remotePath, Boolean recursive) {
    Promise<List<FileStatus>> result = Promise.promise();

    vertx.executeBlocking(promise -> {
      try {
        RemoteIterator<LocatedFileStatus> iterator = this.hdfsFileSystem.listFiles(remotePath, recursive);
        List<FileStatus> fileStatuses = new LinkedList<>();
        while (iterator.hasNext()) {
          LocatedFileStatus fileStatus = iterator.next();
          // Skip Spark Internal Files
          if (fileStatus.getPath().toString().contains("/_spark_metadata")) {
            continue;
          }
          fileStatuses.add(fileStatus);
        }
        promise.complete(fileStatuses);
      } catch (IOException e) {
        String message = "Could not retrieve FileStatuses";
        LOGGER.error(message);
        promise.fail(message);
      }
    }, result);

    return result.future();
  }

  /**
   * List all Files and Folder of a given Path
   *
   * @param remotePath the path
   * @return List of FileStatuses for all Files and Folder included in the Path
   */
  public Future<List<FileStatus>> getFileStatus(Path remotePath) {
    Promise<List<FileStatus>> remoteFiles = Promise.promise();
    vertx.executeBlocking(promise -> {
      try {
        if (hdfsFileSystem.exists(remotePath)) {
          FileStatus[] fileStatuses = hdfsFileSystem.listStatus(remotePath);
          promise.complete(Arrays.asList(fileStatuses));
        } else {
          promise.complete(new LinkedList<>());
        }
      } catch (IOException e) {
        String message = "Could not get FileStatus from HDFS";
        promise.fail(message);
        LOGGER.error(message);
        LOGGER.debug(e.getMessage());
      }
    }, remoteFiles);

    return remoteFiles.future();
  }

  /**
   * List all Files and Folder of a given Path with filtering
   *
   * @param remotePath the path
   * @param pathFilter the Filter for the Files
   * @return List of FileStatuses forFiles and Folder included in the Path
   */
  public Future<List<FileStatus>> getFileStatus(Path remotePath, PathFilter pathFilter) {
    Promise<List<FileStatus>> remoteFiles = Promise.promise();
    vertx.executeBlocking(promise -> {
      try {
        if (hdfsFileSystem.exists(remotePath)) {
          FileStatus[] fileStatuses = hdfsFileSystem.listStatus(remotePath, pathFilter);
          promise.complete(Arrays.asList(fileStatuses));
        } else {
          promise.complete(new LinkedList<>());
        }
      } catch (IOException e) {
        String message = "Could not get FileStatus from HDFS";
        promise.fail(message);
        LOGGER.error(message);
        LOGGER.debug(e.getMessage());
      }
    }, remoteFiles);

    return remoteFiles.future();
  }

  /**
   * Creates for a file on the localFileSystem and uploads it to the hdfs Upload Folder
   *
   * @param localFilePath Path to LocalFile
   * @param fileName      Filename
   * @return Id of the uploaded File
   */
  public Future<String> uploadFile(String localFilePath, String fileName) {
    Promise<String> result = Promise.promise();
    newFileId()
            .onSuccess(result::complete)
      .onFailure(error -> LOGGER.error("Could not get File ID", error))
      .flatMap(id -> uploadFile(localFilePath, fileName, id))
      .onSuccess(info -> LOGGER.debug("File Upload complete"))
      .onFailure(error -> LOGGER.error("Could not upload file to HDFS", error));

    return result.future();
  }

  /**
   * uploads it to the hdfs Upload Folder with given fileId
   *
   * @param localFilePath Path to LocalFile
   * @param fileName      Filename
   * @param fileId        file id in hdfs Upload Folder
   * @return file id in hdfs Upload Folder
   */
  public Future<String> uploadFile(String localFilePath, String fileName, String fileId) {
    Promise<String> id = Promise.promise();

    vertx.executeBlocking(promise -> {
      Path localPath = new Path(localFilePath);
      Path remotePath = new Path(fileId + "/" + fileName);

      try {
        this.hdfsFileSystem.setWorkingDirectory(new Path(this.hdfsStructure.uploadDir));
        this.hdfsFileSystem.moveFromLocalFile(localPath, remotePath);
        promise.complete(fileId);
      } catch (IOException e) {
        promise.fail(e.getCause());
        LOGGER.error("Could not upload File: " + localFilePath, e);
        LOGGER.debug(e.getMessage());
      }

    }, id);

    return id.future();

  }

  /**
   * Downloads a File from hdfs to the Local Download Folder given in the Config
   * The local file will be renamed with a random id.
   *
   * @param remotePath Path to the File in hdfs
   * @return the name of the File in the local Download Folder
   */
  public Future<String> downloadFile(Path remotePath) {
    Promise<String> localFilePath = Promise.promise();
    vertx.executeBlocking(promise -> {
      String fileName = UUID.randomUUID().toString();
      String localPath = config.getString(Constants.HDFS_LOCAL_DOWNLOAD_FOLDER) + "\\" + fileName;
      try {
        FSDataInputStream fsDataInputStream = hdfsFileSystem.open(remotePath);
        Files.copy(fsDataInputStream.getWrappedStream(), Paths.get(localPath));
        promise.complete(localPath);
        fsDataInputStream.close();
      } catch (IOException e) {
        LOGGER.error("Could not download File");
        promise.fail("Could not downlod File");
      }
    }, localFilePath);

    return localFilePath.future();
  }

  /**
   * Downloads a List of Files from hdfs to the Local Download Folder given in the Config
   * and merges it into one file.
   * The local file will be renamed with a random id.
   *
   * @param files List of Filestatuses to download
   * @return the name of the File in the local Download Folder
   */
  public Future<String> downloadMerge(List<FileStatus> files) {
    Promise<String> localFilePath = Promise.promise();
    vertx.executeBlocking(promise -> {
      String fileName = UUID.randomUUID().toString();
      String localPath = config.getString(Constants.HDFS_LOCAL_DOWNLOAD_FOLDER) + "\\" + fileName;

      try (OutputStream out = new FileOutputStream(localPath)) {
        files.sort((fs1, fs2) -> fs1.getModificationTime() > fs2.getModificationTime() ? 1 : -1);
        for (FileStatus file : files) {
          FSDataInputStream in = hdfsFileSystem.open(file.getPath());
          out.write(StreamUtils.getBytes(in.getWrappedStream()));
          in.close();
        }
        localFilePath.complete(localPath);
      } catch (IOException e) {
        localFilePath.fail("Could not DownloadMerge Files");
        LOGGER.error("Couldn not DownloadMerge Files");
      }

    }, localFilePath);

    return localFilePath.future();
  }

  /**
   * Delete file on hdfs Filesystem
   *
   * @param remotePath Path to the File to be deleted
   * @return Future of true if the file could be successfully deleted, false otherwise
   */
  public Future<Boolean> deleteFile(Path remotePath) {
    Promise<Boolean> fileDeleted = Promise.promise();

    vertx.executeBlocking(promise -> {
      try {
        promise.complete(hdfsFileSystem.delete(remotePath, true));
      } catch (IOException e) {
        String message = remotePath.getName();
        promise.fail(message);
        LOGGER.info(message);
        LOGGER.debug(e.getMessage());
      }
    }, fileDeleted);

    return fileDeleted.future();
  }

  /**
   * Creates a new FileId for the Upload Folder on HDFS
   *
   * @return Future of the newFileID
   */
  public Future<String> newFileId() {
    Promise<String> fileId = Promise.promise();
    Promise<Boolean> idIsFree = Promise.promise();
    String newId = UUID.randomUUID().toString();

    vertx.executeBlocking(promise -> {
      try {
        promise.complete(!hdfsFileSystem.exists(new Path(newId)));
      } catch (IOException e) {
        promise.fail(e.getCause());
        LOGGER.error("Could not create new FileId");
        LOGGER.debug(e.getMessage());
      }
    }, idIsFree);

    idIsFree
      .future()
      .flatMap(isFree -> isFree ? Future.succeededFuture(newId) : newFileId())
            .onSuccess(fileId::complete)
            .onFailure(error -> LOGGER.error("File Id not created" ,error ));

    return fileId.future();
  }


  /**
   * Pumps/Streams one File from an HDFS to a vertx WriteStream
   *
   * @param remotePath  Path to the hdfs File
   * @param writeStream Stream to pump the File to
   * @return Future succeeds when the File has been successfully pumped and fails otherwise
   */
  public Future<Void> pumpFile(Path remotePath, WriteStream<io.vertx.core.buffer.Buffer> writeStream, boolean skipFirstLine) {
    Promise<Void> result = Promise.promise();
    FSDataInputStream fsDataInputStream;
    try {
      fsDataInputStream = hdfsFileSystem.open(remotePath);
      if (skipFirstLine) readFirstLine(fsDataInputStream);
      AsyncInputStream asyncInputStream = new AsyncInputStream(vertx, vertx.nettyEventLoopGroup(), fsDataInputStream);
      Pump filePump = Pump.pump(asyncInputStream, writeStream);
      asyncInputStream.endHandler(result::complete);
      asyncInputStream.exceptionHandler(error -> result.fail("Could not pump File: " + remotePath.getName()));
      filePump.start();
    } catch (IOException e) {
      result.fail("Could not open File: " + remotePath.getName());
      LOGGER.error("Could not open File: " + remotePath.getName());
      LOGGER.debug(e);
    }

    return result.future();
  }

  public Future<Void> pumpFile(Path remotePath, WriteStream<io.vertx.core.buffer.Buffer> writeStream) {
    return pumpFile(remotePath, writeStream, false);
  }

  /**
   * Pumps/Streams a list of Files from an Hdfs to a vertx WriteStream in the given order
   *
   * @param remotePaths Paths to the Hdfs File
   * @param writeStream Stream to pump the Files to
   * @return Future succeeds when all Files has been successfully pumped and fails otherwise
   */
  public Future<Void> pumpFiles(List<Path> remotePaths, WriteStream<io.vertx.core.buffer.Buffer> writeStream, DataFormat format) {
    if (remotePaths.size() == 0)
      return Future.succeededFuture();

    if (format == DataFormat.CSV) {
      return pumpFile(remotePaths.remove(0), writeStream, false).flatMap(aVoid -> pumpFiles(remotePaths, writeStream, true));
    }
    return pumpFile(remotePaths.remove(0), writeStream, false).flatMap(aVoid -> pumpFiles(remotePaths, writeStream, false));
  }

  private Future<Void> pumpFiles(List<Path> remotePaths, WriteStream<io.vertx.core.buffer.Buffer> writeStream, boolean skipFirstLine) {
    if (remotePaths.size() > 0) {
      return pumpFile(remotePaths.remove(0), writeStream, skipFirstLine).flatMap(aVoid -> pumpFiles(remotePaths, writeStream, skipFirstLine));
    }
    return Future.succeededFuture();
  }

  private Future<Void> openHDFSConnection() {
    Promise<Void> connectionOpen = Promise.promise();
      try {
        Configuration hdfsConfig = ConfigRetrieverOptionsProvider.hdfsConfig(config);
        this.hdfsFileSystem = FileSystem.get(hdfsConfig);
        this.hdfsFileSystem.setWorkingDirectory(new Path(this.hdfsStructure.uploadDir));
        connectionOpen.complete();
      } catch (IOException e) {
        LOGGER.error("Could not instanciate HDFS");
        LOGGER.debug(e.getMessage());
        connectionOpen.fail(e);
      }
    return connectionOpen.future();
  }

  private Future<Void> hdfsClientHealthCheck() {
    LOGGER.debug("Performing Healthcheck on HDFS Client");
    Promise<Void> finished = Promise.promise();
    hdfsCheckConnectionTime()
      .flatMap(connectionTimeMs -> {
        if (connectionTimeMs > connectionTimeTreshholdMs)
          return Future.failedFuture("Connection Time bigger than Threshold: " + connectionTimeMs);
        return Future.succeededFuture();

      })
      .onSuccess(success -> finished.complete())
      .onFailure(error -> {
        LOGGER.error(error);
        openHDFSConnection();
        finished.complete();
      });
    return finished.future();
  }

  private Future<Long> hdfsCheckConnectionTime() {
    Promise<Long> result = Promise.promise();
    vertx.executeBlocking(promise -> {
        long start = System.nanoTime();
        long elapsedTimeMs = Long.MAX_VALUE;
        try {
          this.hdfsFileSystem.getStatus();
          elapsedTimeMs = (System.nanoTime() - start) / 1000000;
        } catch (Exception e) {
          LOGGER.debug(e);
          LOGGER.error("Error with HDFS Connection");
          e.printStackTrace();
          promise.fail("Error with hdfs connection");
        }
        promise.complete(elapsedTimeMs);
      },
      result);
    return result.future();
  }

  @Override
  public void close(Promise<Void> handler) {
    if (this.hdfsHealthCheckJobId != null) {
      vertx.cancelTimer(hdfsHealthCheckJobId);
    }

    handler.handle(Future.succeededFuture());
  }

  /**
   * Reads first line of an inputstream
   * @param in The input stream to read a line from
   */
  private void readFirstLine(InputStream in) {
    int i;
    try {
      while ((i = in.read()) != -1) {
        if (i == (byte) '\n')
          break;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
