package io.piveau.DataStore.Connector.util;

import io.piveau.DataStore.Connector.MainVerticle;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.hadoop.conf.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ConfigRetrieverOptionsProvider {
  private static final int scanPeriod = 2000;
  private static final String environment = System.getenv(Constants.ENVIRONMENT_ENV_VARIABLE);
  private static final String kafkaConsumerUUID = UUID.randomUUID().toString();
  public static ConfigRetrieverOptions options = new ConfigRetrieverOptions()
    .setScanPeriod(scanPeriod)
    .addStore(defaultPropertyStore())
    .addStore(propertyStore())
    .addStore(environmentStore());

  /**
   * Parse the Config for a  KafkaProducer Verticle out of a vertx config file
   *
   * @param config the config file
   * @return Kafka Producer config
   */
  public static Map<String, String> kafkaProducerConfig(JsonObject config) {
    Map<String, String> result = new HashMap<>();
    result.put("bootstrap.servers", config.getString(Constants.KAFKA_SERVERS));
    result.put("key.serializer", config.getString(Constants.KAFKA_KEY_SERIALIZER));
    result.put("value.serializer", config.getString(Constants.KAFKA_VALUE_SERIALIZER));
    result.put("metadata.max.age.ms", config.getString(Constants.KAFKA_METADATA_MAX_AGE));
    return result;
  }

  /**
   * Parse the Config for a  KafkaConsumer Verticle out of a vertx config file
   *
   * @param config the config file
   * @return Kafka Producer config
   */
  public static Map<String, String> kafkaConsumerConfig(JsonObject config) {
    Map<String, String> result = new HashMap<>();
    result.put("bootstrap.servers", config.getString(Constants.KAFKA_SERVERS));
    result.put("key.deserializer", config.getString(Constants.KAFKA_KEY_DESERIALIZER));
    result.put("value.deserializer", config.getString(Constants.KAFKA_VALUE_DESERIALIZER));
    result.put("auto.offset.reset", config.getString(Constants.KAFKA_AUTO_OFFSET_RESET));
    result.put("enable.auto.commit", config.getBoolean(Constants.KAFKA_ENABLE_AUTO_COMMIT).toString());
    result.put("metadata.max.age.ms", config.getInteger(Constants.KAFKA_METADATA_MAX_AGE).toString());
    result.put("group.id", MainVerticle.class.getCanonicalName() + "." + kafkaConsumerUUID);

    return result;
  }

  /**
   * Parse the Config for a hdfs Connection out of a vertx config file
   *
   * @param config the config file
   * @return the hdfs Config
   */
  public static Configuration hdfsConfig(JsonObject config) {
    // Create HDFS Config
    Configuration conf = new Configuration();
    conf.set("fs.defaultFS", config.getString(Constants.HDFS_HOST));
    conf.set("dfs.client.use.datanode.hostname", "true");
    // Solve HDFS Path Issues https://stackoverflow.com/questions/17265002/hadoop-no-filesystem-for-scheme-file
    conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
    conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());

    return conf;
  }

  // Property Stores

  /**
   * Loads the ConfigStoreOptions for the default.properties File
   *
   * @return the ConfigStoreOptions
   */
  private static ConfigStoreOptions defaultPropertyStore() {
    return new ConfigStoreOptions()
      .setType("file")
      .setConfig(new JsonObject()
          .put("path", "default.json")
        //.put("hierarchical", true)
      );
  }

  /**
   * Loads the ConfigStoreOptions for Production or Development
   * based on the environment Variable 'env'
   *
   * @return the ConfigStoreOptions
   */
  private static ConfigStoreOptions propertyStore() {
    String propertyPath = "dev.json";
    if (environment != null && environment.equals("prod"))
      propertyPath = "prod.json";

    return new ConfigStoreOptions()
      .setType("file")
      .setConfig(new JsonObject()
        .put("path", propertyPath)
      );
  }


  /**
   * Loads the ConfigStoreOptions for Production or Development
   * based on the environment Variable 'env'
   *
   * @return the ConfigStoreOptions
   */
  private static ConfigStoreOptions environmentStore(){
    return new ConfigStoreOptions()
      .setType("env")
      .setConfig(new JsonObject().put("keys", new JsonArray()
          .add(Constants.API_KEY)
          .add(Constants.ENVIRONMENT_ENV_VARIABLE)
          .add(Constants.ADVERTISED_TOPICS)

        )
      );
  }

}
