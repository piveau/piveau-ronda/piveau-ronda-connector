package io.piveau.DataStore.Connector.util;

import io.piveau.DataStore.Connector.handler.DatasetHandler;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.impl.Closeable;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DataStoreUtils {
  private static final Logger LOGGER = Logger.getLogger(DatasetHandler.class);

  Vertx vertx;
  JsonObject config;

  /**
   * Utils for the DataStore
   *
   * @param vertx  the Vertx Instance
   * @param config the Vertx Configuraiton
   */
  public DataStoreUtils(Vertx vertx, JsonObject config) {
    this.vertx = vertx;
    this.config = config;
  }

  // PATH

  /**
   * Pagination for Java Lists
   *
   * @param sourceList The java List to be paginated
   * @param page The page number
   * @param pageSize The size of one Page
   * @param <T> The type of the List
   * @return Paginated List
   */
  public static <T> List<T> getPage(List<T> sourceList, int page, int pageSize) {

    if (pageSize <= 0 || page <= 0) {
      throw new IllegalArgumentException("invalid page size: " + pageSize);
    }

    int fromIndex = (page - 1) * pageSize;
    if (sourceList == null || sourceList.size() < fromIndex) {
      return Collections.emptyList();
    }

    return sourceList.subList(fromIndex, Math.min(fromIndex + pageSize, sourceList.size()));
  }


  /**
   * Deletes a Local File
   * @param filePath Path to the File
   * @return Future when File is deleted
   */
  public Future<Void> deleteLocalFile(String filePath) {
    Promise<Void> deleted = Promise.promise();

    vertx.fileSystem().delete(filePath, deleted);

    return deleted.future();
  }

  /**
   * Deletes a Local File file the Last modification is older than the parameter
   * @param filePath Path to th eFile
   * @param olderThan
   * @return Future when File is deleted
   */
  public Future<Boolean> deleteLocalFile(String filePath, Long olderThan) {
    Promise<Boolean> deleted = Promise.promise();
    vertx.executeBlocking(promise -> {
      File file = new File(filePath);
      if (!file.exists()) {
        promise.fail("File does not exist");
      } else if (file.lastModified() - olderThan < 0) {
        LOGGER.info("delete File: " + filePath);
        promise.complete(file.delete());
      } else {
        promise.complete(false);
      }
    }, deleted);

    return deleted.future();
  }

  /**
   * Deletes all temporary Files that or older than the Value given in the Config
   */
  public void deleteTempFiles() {
    LOGGER.debug("Deleting old Temp Files");
    Long olderThan = System.currentTimeMillis()
      - this.config.getInteger(Constants.FILESYSTEM_TEMPFILES_DELETEOLDERTHAN) * 60 * 1000;
    Promise<List<String>> uploadFiles = Promise.promise();
    Promise<List<String>> downloadFiles = Promise.promise();

    vertx.fileSystem()
      .readDir("file-uploads", uploadFiles)
      .readDir(config.getString(Constants.HDFS_LOCAL_DOWNLOAD_FOLDER), downloadFiles)
    ;

    uploadFiles
      .future()
      .map(files -> files
        .stream()
        .map(file -> this.deleteLocalFile(file, olderThan))
        .collect(Collectors.toList())
      );

    downloadFiles
      .future()
      .map(files -> files
        .stream()
        .map(file -> this.deleteLocalFile(file, olderThan))
        .collect(Collectors.toList())
      );

  }

  public static Future<Void> closeIfExists(Closeable closeable){
    Promise<Void> closed = Promise.promise();
    if(closeable != null){
      closeable.close(closed);
    }else{
      closed.complete();
    }
    return closed.future();
  }
}
