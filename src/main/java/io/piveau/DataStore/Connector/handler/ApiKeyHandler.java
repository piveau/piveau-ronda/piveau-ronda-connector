package io.piveau.DataStore.Connector.handler;

import com.google.common.net.HttpHeaders;
import io.piveau.DataStore.Connector.model.httpMessage.HttpMessage;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authentication.AuthenticationProvider;
import io.vertx.ext.auth.authentication.CredentialValidationException;
import io.vertx.ext.auth.authentication.Credentials;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.AuthenticationHandler;

/**
 * The ApiKeyHandler Class implements a simple check for one API Key.
 */
public class ApiKeyHandler implements AuthenticationHandler {

  @Override
  public void handle(RoutingContext context) {
    Promise<Credentials> apiKey = Promise.promise();
    parseCredentials(context, apiKey);
  }

  public class APIKey implements Credentials {
    private String localApiKey;

    public APIKey(String localApiKey){
      this.localApiKey = localApiKey;
    }
    @Override
    public <V> void checkValid(V arg) throws CredentialValidationException {
      if(!apiKey.equals(localApiKey)){
        throw new CredentialValidationException("Api Key is not Valid");
      }
    }

    @Override
    public JsonObject toJson() {
      return null;
    }
  }

  private final String apiKey;

  /**
   *
   * @param apiKey the valid ApiKey
   */
  public ApiKeyHandler(String apiKey) {
    this.apiKey = apiKey;
  }

  @Override
  public void parseCredentials(RoutingContext context, Handler<AsyncResult<Credentials>> handler) {
    final String authorization = context.request().headers().get(HttpHeaders.AUTHORIZATION);

    if(this.apiKey.isEmpty()) {
      HttpMessage response = new HttpMessage();
      response.message = "Api-Key is not specified";
      response.statusCode = 500;
      context.response().putHeader("Content-Type", "application/json");
      context.response().setStatusCode(response.statusCode);
      context.response().end(response.toJson().toBuffer());
    } else if(authorization == null) {
      HttpMessage response = new HttpMessage();
      response.statusCode = 401;
      response.message = "Header field Authorization is missing";
      context.response().putHeader("Content-Type", "application/json");
      context.response().setStatusCode(response.statusCode);
      context.response().end(response.toJson().toBuffer());
    } else if (!authorization.equals(this.apiKey)) {
      HttpMessage response = new HttpMessage();
      response.statusCode = 401;
      response.message = "Incorrect Api-Key";
      context.response().putHeader("Content-Type", "application/json");
      context.response().setStatusCode(response.statusCode);
      context.response().end(response.toJson().toBuffer());
    } else {
      handler.handle(Future.succeededFuture(new APIKey(authorization)));
      context.next();
    }
  }
}
