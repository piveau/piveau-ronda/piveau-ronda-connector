package io.piveau.DataStore.Connector.handler;

import io.piveau.DataStore.Commons.schema.DataFormat;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.base.Topic;
import io.piveau.DataStore.Connector.model.datasetMessage.DatasetMessage;
import io.piveau.DataStore.Connector.services.dataset.DatasetService;
import io.piveau.DataStore.Connector.util.Constants;
import io.piveau.DataStore.Connector.util.DataStoreUtils;
import io.piveau.DataStore.Connector.util.HdfsUtils;
import io.piveau.DataStore.Connector.util.HttpUtils;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class DatasetHandler implements Closeable {
  private static final Logger LOGGER = Logger.getLogger(DatasetHandler.class);

  Vertx vertx;
  JsonObject config;

  DataStoreUtils dataStoreUtils;
  HdfsUtils hdfsUtils;
  DatasetService datasetService;


  Tika tika;

  Long periodicDeleteJobId;

  /**
   * HTTP Handler for Dataset and Windowed Datastream Requests
   *
   * @param config The Vertx Config provided by vertx ConfigRetriever
   * @param vertx  The Vertx Instance
   */
  public DatasetHandler(Vertx vertx, JsonObject config) {
    // Instantiate Class Objects
    this.vertx = vertx;
    this.config = config;
    this.dataStoreUtils = new DataStoreUtils(vertx, config);
    this.tika = new Tika();

    Promise<HdfsUtils> hdfsPromise = Promise.promise();
    this.hdfsUtils = new HdfsUtils(vertx, config, hdfsPromise);

    Promise<DatasetService> datasetServicePromise= Promise.promise();
    this.datasetService = DatasetService.create(config, vertx, datasetServicePromise);

    // Periodically delete idle temporary files
    int periodicMinutes = this.config.getInteger(Constants.FILESYSTEM_TEMPFILES_DELETEOLDERTHAN) * 60 * 1000;
    periodicDeleteJobId = vertx.setPeriodic(periodicMinutes, id -> dataStoreUtils.deleteTempFiles());

  }


  /**
   * HTTP Mehtod to return a simple Upload Form for the DataStore to the given RoutingContext
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void getDatasetForm(RoutingContext context) {
    Promise<String> htmlPromise = Promise.promise();
    this.datasetService.getDatasetForm(htmlPromise);
    context.response().putHeader("content-type", "text/html");
    context.response().sendFile("webroot/simpleUploadForm.html");
  }

  /**
   * HTTP Method to return a List of relative Locations to uploaded Datasets
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void getDatasetList(RoutingContext context) {
    // Parse Arguments
    String path = HttpUtils.normalizedPath(context.normalisedPath());
    int page = 1; // Default Value
    try {
      page = Integer.parseInt(context.queryParam("p").get(0));
    } catch (Exception ignored) {
      LOGGER.debug("No Page given for this Request. Using default Value.");
    }

    int pageSize = config.getInteger(Constants.DEFAULT_PAGESIZE); // Default Value
    try {
      int parsedPageSize = Integer.parseInt(context.queryParam("pageSize").get(0));
      pageSize = Math.min(parsedPageSize, config.getInteger(Constants.MAX_PAGESIZE));
    } catch (Exception ignored) {
      LOGGER.debug("No PageSize given for this Request. Using default Value.");
    }

    Promise<DatasetMessage> jsonPromise = Promise.promise();
    this.datasetService.getDatasetLocations(page, pageSize, jsonPromise);
    jsonPromise
      .future()
      .map(datasetMessage -> datasetMessage.setLocationPrefix(path + "/"))
      .onSuccess(datasetMessage -> {
        context.response()
          .setStatusCode(datasetMessage.statusCode)
          .end(datasetMessage.toJson().encode());
      })
      .onFailure(error -> HttpUtils.internalServerError(context));
  }

  /**
   * HTTP Method to return a DatasetFile
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void getDataset(RoutingContext context) {
    // Parse Arguments
    String datasetId = context.pathParam("datasetId");

    Promise<DatasetMessage> metaDataPromise = Promise.promise();
    this.datasetService.getDatasetMetadata(datasetId, metaDataPromise);

    metaDataPromise
      .future()
      .flatMap(datasetMessage -> {
        Promise<DatasetMessage> result = Promise.promise();
        if (datasetMessage.statusCode == 200) {
          String fileName = datasetMessage.fileNames.get(0);
          String fileLength = datasetMessage.fileSizes.get(0).toString();
          String mimeType = tika.detect(fileName);

          context.response()
            .setChunked(true)
            .putHeader("content-type", mimeType)
            .putHeader("content-length", fileLength)
            .putHeader("Content-Disposition", "attachment; filename=" + fileName);

          this.hdfsUtils.pumpFile(new Path(datasetId, fileName), context.response())
            .onSuccess(aVoid -> result.complete(datasetMessage))
            .onFailure(result::fail);
        } else {
          result.complete(datasetMessage);
        }

        return result.future();
      })
      .onSuccess(datasetLocation -> {
        if (datasetLocation.statusCode == 200) {
          context
            .response()
            .end();
        } else {
          context
            .response()
            .setStatusCode(datasetLocation.statusCode)
            .end(datasetLocation.toJson().encode());
        }
      })
      .onFailure(error -> HttpUtils.internalServerError(context));
  }

  /**
   * HTTP Method to upload a DatasetFile
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void postDataset(RoutingContext context) {
    String path = HttpUtils.normalizedPath(context.normalisedPath());
    LOGGER.info("post");
    if (context.fileUploads().size() != 1) {
      DatasetMessage result = new DatasetMessage()
        .statusCode(400).message("Exactly 1 File is accepted per Upload");

      context
        .response()
        .setStatusCode(result.statusCode)
        .end(result.toJson().encode());
      return;
    }
    FileUpload fileUpload = context.fileUploads().iterator().next();
    if (fileUpload.fileName().isEmpty()) {
      DatasetMessage result = new DatasetMessage()
        .statusCode(400).message("The provided file has no name");

      context
        .response()
        .setStatusCode(result.statusCode)
        .end(result.toJson().encode());
      return;
    }
    Promise<DatasetMessage> locationPromise = Promise.promise();
    this.datasetService.uploadDataset(fileUpload.uploadedFileName(), fileUpload.fileName(), locationPromise);
    locationPromise
      .future()
      .map(location -> location.setLocationPrefix(path + "/"))
      .onSuccess(location -> {
        context
          .response()
          .putHeader("Location", location.datasetLocations.get(0))
          .setStatusCode(location.statusCode)
          .end(location.toJson().encode());

      })
      .onFailure(error -> HttpUtils.internalServerError(context));
  }

  /**
   * HTTP Method to update an existing DatasetFile
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void putDataset(RoutingContext context) {
    String rootPath = HttpUtils.normalizedPath(context.normalisedPath()).replaceFirst("/[^/]+$", "");
    if (context.fileUploads().size() != 1) {
      DatasetMessage result = new DatasetMessage()
        .statusCode(400).message("Only exactly 1 File is accepted per Upload");

      context
        .response()
        .setStatusCode(result.statusCode)
        .end(result.toJson().encode());
      return;
    }

    String datasetId = context.pathParam("datasetId");
    FileUpload fileUpload = context.fileUploads().iterator().next();
    if (fileUpload.fileName().isEmpty()) {
      DatasetMessage result = new DatasetMessage()
        .statusCode(400).message("The provided file has no name");

      context
        .response()
        .setStatusCode(result.statusCode)
        .end(result.toJson().encode());
      return;
    }

    String filePath = fileUpload.uploadedFileName();
    String fileName = fileUpload.fileName();

    Promise<DatasetMessage> locationPromise = Promise.promise();
    this.datasetService.replaceDataset(datasetId, filePath, fileName, locationPromise);
    locationPromise
      .future()
      .map(location -> location.setLocationPrefix(rootPath + "/"))
      .onSuccess(location -> {
        context
          .response()
          .putHeader("Location", location.datasetLocations.get(0))
          .setStatusCode(location.statusCode)
          .end(location.toJson().encode());

      })
      .onFailure(error -> HttpUtils.internalServerError(context));
  }

  /**
   * HTTP Method to delete an existing DatasetFile
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void deleteDataset(RoutingContext context) {
    String datasetId = context.pathParam("datasetId");
    String rootPath = HttpUtils.normalizedPath(context.normalisedPath()).replaceFirst("/[^/]+$", "");
    Promise<DatasetMessage> deletedPromise = Promise.promise();
    this.datasetService.deleteDataset(datasetId, deletedPromise);
    deletedPromise
      .future()
      .map(deleted -> deleted.setLocationPrefix(rootPath + "/"))
      .onSuccess(deleted -> {
        context.response()
          .setStatusCode(deleted.statusCode)
          .end(deleted.toJson().encode());
      })
      .onFailure(error -> HttpUtils.internalServerError(context));
  }

  /**
   * HTTP Method to return a List of relative Locations to existing DataStreams
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void getDataStreamList(RoutingContext context) {
    // Parse Query Params
    String path = HttpUtils.normalizedPath(context.normalisedPath());
    int page = 1; // Default Value
    try {
      page = Integer.parseInt(context.queryParam("p").get(0));
    } catch (Exception ignored) {
      LOGGER.debug("No Page given for this Request. Using default Value.");
    }

    int pageSize = config.getInteger(Constants.DEFAULT_PAGESIZE); // Default Value
    try {
      int parsedPageSize = Integer.parseInt(context.queryParam("pageSize").get(0));
      pageSize = Math.min(parsedPageSize, config.getInteger(Constants.MAX_PAGESIZE));
    } catch (Exception ignored) {
      LOGGER.debug("No PageSize given for this Request. Using default Value.");
    }

    Promise<DatasetMessage> messagePromise = Promise.promise();
    this.datasetService.getDataStreamList(page, pageSize, messagePromise);

    messagePromise
      .future()
      .map(datasetMessage -> datasetMessage.setLocationPrefix(path + "/"))
      .onSuccess(datasetMessage -> {
        context.response()
          .setStatusCode(datasetMessage.statusCode)
          .end(datasetMessage.toJson().encode());
      })
      .onFailure(error -> HttpUtils.internalServerError(context));

  }

  /**
   * HTTP Method to return a Datastream window as a CSV File
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void getDataStream(RoutingContext context) {
    // Parse Parameters
    String datasetId = context.pathParam("dataStreamId");

    LocalDate currentDate = LocalDate.now();
    int year = context.queryParam("year").size() > 0 ? Integer.parseInt(context.queryParam("year").get(0)) : currentDate.getYear();
    int month = context.queryParam("month").size() > 0 ? Integer.parseInt(context.queryParam("month").get(0)) : currentDate.getMonthValue();
    int day = context.queryParam("day").size() > 0 ? Integer.parseInt(context.queryParam("day").get(0)) : -1;
    String queryFormat = context.queryParam("format").size() > 0 ? context.queryParam("format").get(0) : null;
    Promise<DatasetMessage> locationPromise = Promise.promise();

    String acceptableContentType = null;
    if(queryFormat != null) {
      acceptableContentType = queryFormat.toLowerCase().equals("csv") ? "text/csv" : "application/json";
    }else {
      acceptableContentType = context.getAcceptableContentType() != null ? context.getAcceptableContentType() :
              "application/json";

    }
    DataFormat format = DataFormat.byMimeType(acceptableContentType);
    if (format == null) {
      context.response().end(new DatasetMessage().statusCode(406)
        .message("Format not supported for this Datastream")
        .toJson()
        .toBuffer());
      return;
    }

    if (day > 0) {
      this.datasetService.getMetadataDataStreamDay(datasetId, year, month, day, format, locationPromise);
    } else {
      this.datasetService.getMetadataDataStreamMonth(datasetId, year, month, format, locationPromise);
    }

    locationPromise
      .future()
      .flatMap(datasetMessage -> {
        Promise<DatasetMessage> result = Promise.promise();
        if (datasetMessage.statusCode == 200) {
          String mimeType = tika.detect(datasetMessage.fileNames.get(0));
          context
            .response()
            .setChunked(true)
            .setStatusCode(datasetMessage.statusCode)
            .putHeader("content-type", mimeType)
            .putHeader("Content-Disposition", "attachment; filename=" + datasetMessage.fileNames.get(0));

          hdfsUtils.pumpFiles(datasetMessage.datasetLocations.stream().map(Path::new).collect(Collectors.toList())
            , context.response(), format)
            .onSuccess(aVoid -> result.complete(datasetMessage))
            .onFailure(result::fail);
        } else {
          result.complete(datasetMessage);
        }

        return result.future();
      })
      .onSuccess(location -> {
        if (location.statusCode == 200) {
          context.response().end();
        } else {
          context.response().setStatusCode(location.statusCode).end(location.toJson().encode());
        }
      })
      .onFailure(error -> HttpUtils.internalServerError(context));
  }

  /**
   * HTTP Method to return a Datastream window as a CSV File
   *
   * @param context The HTTP RoutingContext for the response
   */
  public void headDataStream(RoutingContext context) {
    // Parse Parameters
    String datasetId = context.pathParam("dataStreamId");

    LocalDate currentDate = LocalDate.now();
    int year = context.queryParam("year").size() > 0 ? Integer.parseInt(context.queryParam("year").get(0)) : currentDate.getYear();
    int month = context.queryParam("month").size() > 0 ? Integer.parseInt(context.queryParam("month").get(0)) : currentDate.getMonthValue();
    int day = context.queryParam("day").size() > 0 ? Integer.parseInt(context.queryParam("day").get(0)) : -1;
    String queryFormat = context.queryParam("format").size() > 0 ? context.queryParam("format").get(0) : null;
    Promise<DatasetMessage> locationPromise = Promise.promise();

    String acceptableContentType = null;
    if(queryFormat != null) {
      acceptableContentType = queryFormat.toLowerCase().equals("csv") ? "text/csv" : "application/json";
    }else {
      acceptableContentType = context.getAcceptableContentType() != null ? context.getAcceptableContentType() :
              "application/json";

    }
    DataFormat format = DataFormat.byMimeType(acceptableContentType);
    if (format == null) {
      context.response().end(new DatasetMessage().statusCode(406)
              .message("Format not supported for this Datastream")
              .toJson()
              .toBuffer());
      return;
    }

    if (day > 0) {
      this.datasetService.getMetadataDataStreamDay(datasetId, year, month, day, format, locationPromise);
    } else {
      this.datasetService.getMetadataDataStreamMonth(datasetId, year, month, format, locationPromise);
    }

    locationPromise
            .future()
            .onSuccess(location -> {
                context.response().putHeader("content-length", String.valueOf(location.fileSizes.stream().mapToInt(Long::intValue).sum()))
                        .setStatusCode(location.statusCode)
                        .end(location.toJson().encode());

            })
            .onFailure(error -> HttpUtils.internalServerError(context));
  }

  @Override
  public void close(Promise<Void> closed) {
    if (this.periodicDeleteJobId != null) {
      vertx.cancelTimer(periodicDeleteJobId);
    }
    Promise<Void> hdfsClosed = Promise.promise();
    if (this.hdfsUtils != null) {
      this.hdfsUtils.close(hdfsClosed);
    } else {
      hdfsClosed.complete();
    }
    Promise<Void> datasetServiceClosed = Promise.promise();
    if (this.datasetService != null) {
      this.datasetService.close(datasetServiceClosed);
    } else {
      datasetServiceClosed.complete();
    }

    CompositeFuture.all(hdfsClosed.future(), datasetServiceClosed.future())
      .onSuccess(success -> closed.complete())
      .onFailure(closed::fail);
  }
}
