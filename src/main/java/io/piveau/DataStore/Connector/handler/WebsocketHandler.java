package io.piveau.DataStore.Connector.handler;

import io.netty.handler.codec.http.QueryStringDecoder;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Connector.util.ConfigRetrieverOptionsProvider;
import io.piveau.DataStore.Connector.util.Constants;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.json.schema.Schema;
import io.vertx.json.schema.SchemaParser;
import io.vertx.json.schema.SchemaRouter;
import io.vertx.json.schema.SchemaRouterOptions;
import io.vertx.kafka.client.common.PartitionInfo;
import io.vertx.kafka.client.consumer.KafkaConsumer;

import io.vertx.kafka.client.consumer.KafkaConsumerRecord;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WebsocketHandler implements Closeable {

  private static final Logger LOGGER = Logger.getLogger(WebsocketHandler.class);
  private static final String openDataPrefix = "OpenData";

  Vertx vertx;
  JsonObject config;
  KafkaConsumer<String, String> kafkaConsumer;
  AtomicReference<Map<String, List<Pair<ServerWebSocket, Schema>>>> topicWebsocketMap;
  Long findNewTopicsJobId;
  Long pingAllWebsocketJobId;
  SchemaRouter schemaRouter;
  SchemaParser schemaParser;

  List<String> advertisedTopics;

  /**
   * Handler for WebSocket Streaming of incoming DataStream data
   * @param vertx The Vertx Instance
   * @param config The Vertx Config
   */
  public WebsocketHandler(Vertx vertx, JsonObject config) {
    this.vertx = vertx;
    this.config = config;

    this.advertisedTopics = config
            .getJsonArray(Constants.ADVERTISED_TOPICS, new JsonArray(KafkaTopics.openDataTopics().stream()
                    .map(t -> t.topicName)
                    .collect(Collectors.toList())
            ))
            .stream()
            .map(s -> (String) s)
            .collect(Collectors.toList());

    int updateTopicIntervalInSeconds = config.getInteger(Constants.STREAMING_UPDATE_TOPICLIST_SECONDS);


    kafkaConsumer = KafkaConsumer.create(this.vertx, ConfigRetrieverOptionsProvider.kafkaConsumerConfig(config));
    schemaRouter = SchemaRouter.create(vertx, new SchemaRouterOptions());
    schemaParser = SchemaParser.createDraft201909SchemaParser(schemaRouter);

    topicWebsocketMap = new AtomicReference<>(new HashMap<>());

    // Start Periodic task to look for new Open Data Topics
    findNewTopicsJobId = vertx.setPeriodic(1000 * updateTopicIntervalInSeconds, id -> updateTopicMap());
    pingAllWebsocketJobId = vertx.setPeriodic(30000, id -> pingAllWebsockets());

    // Handle Kafka Message
    kafkaConsumer
      .subscribe(Pattern.compile(KafkaTopics.OPENDATA_PREFIX + ".*"));
    kafkaConsumer
      .handler(this::kafkaMessageToWebsockets);
  }

  /**
   * Handles new Incoming Websocket Request.
   *
   * @param ws the new Websocket Request
   */
  public void handleWebsocket(ServerWebSocket ws) {
    String path = ws.path();
    // Reject Wrong Path
    if (!path.startsWith("/datastream")) {
      ws.reject();
      return;
    }
    // Parse Topic Name
    String[] topic = ws.path().split("/datastream/");
    String subscribedTopic = topic.length > 1 ? topic[1] : "";

    // Check if Topic actually exists
    if (!topicWebsocketMap.get().containsKey(subscribedTopic)) { // Topic does not exist
      ws.accept();
      String validTopics = topicWebsocketMap.get().keySet().stream()
        .filter(t -> this.advertisedTopics.contains(t))
        .map(t -> "'" + "/datastream/" + t + "'").collect(Collectors.joining(", "));
      ws.writeTextMessage("Stream does not exist. Try one of the following streams: "
          + validTopics,
        voidAsyncResult -> ws.close((short) 404));
    } else { // Topic exists
      //Parse Json Schema
      try {
        Schema jsonSchema = parseJsonSchema(ws.uri());
        Pair<ServerWebSocket, Schema> websocketRequest = new ImmutablePair<>(ws, jsonSchema);
        topicWebsocketMap.get().get(subscribedTopic).add(websocketRequest);
        ws.closeHandler(aVoid -> topicWebsocketMap.get().get(subscribedTopic).remove(websocketRequest));
        ws.drainHandler(aVoid -> topicWebsocketMap.get().get(subscribedTopic).remove(websocketRequest));

      }catch (DecodeException exception){
        ws.accept();
        ws.writeTextMessage("The query you have given is invalid. Please use a valid Json Schema: https://json-schema.org/",
                voidAsyncResult -> ws.close((short) 404));
      }
    }
  }

  /**
   * Filter Out Only Open Data Topics
   * @param topics Topics to be Filtered
   * @return The OpenData Topics
   */
  private Future<Set<String>> filterOpenDataTopics(Set<String> topics) {
    Promise<Set<String>> result = Promise.promise();

    Set<String> outbounds = topics.stream().filter(topic -> topic.startsWith(openDataPrefix + "."))
      .collect(Collectors.toSet());
    result.complete(outbounds);

    return result.future();
  }

  /**
   * Create Map out of a Set of Kafka Topics to register the subscribed Websockets to
   * @param topics the topics
   * @return Future of Topic Websocket HashMap
   */
  private Future<Map<String, List<Pair<ServerWebSocket,Schema>>>> createTopicHashMap(Set<String> topics) {
    Promise<Map<String, List<Pair<ServerWebSocket,Schema>>>> result = Promise.promise();
    Map<String, List<Pair<ServerWebSocket,Schema>>> topicToWebsocket = new HashMap<>();
    // All Topics
    topicToWebsocket.put("", new LinkedList<>());
    for (String topic : topics) {
      topicToWebsocket.put(topic, new LinkedList<>());
    }
    result.complete(topicToWebsocket);
    return result.future();
  }

  /**
   * Periodically looks for new Topics and updates topicWebsocketMap with new Topics
   */
  private void updateTopicMap(){
    LOGGER.debug("Looking for new Topics");
    Promise<Map<String, List<PartitionInfo>>> newTopics = Promise.promise();
    kafkaConsumer.listTopics(newTopics);
    newTopics
      .future()
      .map(Map::keySet)
      .compose(this::filterOpenDataTopics)
      .compose(this::createTopicHashMap)
      .onSuccess(newTopicMap -> {
        // Update Values
        newTopicMap
                .keySet()
                .parallelStream()
                .filter(key -> !topicWebsocketMap.get().containsKey(key))
                .forEach(key -> {
                  LOGGER.info("Found new Topic: " + key);
                  topicWebsocketMap.get().put(key, newTopicMap.get(key));
                });
      })
      .onFailure(error -> {
        LOGGER.error("Could not update Topic Map", error);
      });
  }

  /**
   * Shares an incoming kafka with the subscribed Web sockets
   * @param record the Kafka Record
   */
  private void kafkaMessageToWebsockets(KafkaConsumerRecord<String, String> record){
    LOGGER.debug("Received Message from Topic:" + record.topic());
    // Send Message To Topic Websockets
    topicWebsocketMap.get()
      .getOrDefault(record.topic(), new LinkedList<>())
      .forEach(wsPair -> {
        ServerWebSocket ws = wsPair.getLeft();
        Schema schema = wsPair.getRight();
        if(schema != null){
          writeToWebsocket(ws,schema, new JsonObject(record.value()));
        }else{
          writeToWebsocket(ws, new JsonObject(record.value()));
        }
      });

    // Send Message to Root Websockets
    topicWebsocketMap.get().getOrDefault("", new LinkedList<>())
      .parallelStream()
      .forEach(wsPair -> {
        ServerWebSocket ws = wsPair.getLeft();
        Schema schema = wsPair.getRight();
        if(schema != null){

          writeToWebsocket(ws,schema, new JsonObject(record.value()));
        }else{
          writeToWebsocket(ws, new JsonObject(record.value()));
        }
      });
  }

  private Schema parseJsonSchema(String url) throws DecodeException {
    List<String> qQuery  = new QueryStringDecoder(url).parameters().getOrDefault("q", new LinkedList<>());

    if(qQuery.isEmpty())
      return null;
    return schemaParser.parseFromString(qQuery.get(0));
  }

  private Future<Void> writeToWebsocket(ServerWebSocket ws, Schema schema, JsonObject message){
    return schema.validateAsync(message)
            .flatMap(aVoid -> writeToWebsocket(ws, message));
  }

  private Future<Void> writeToWebsocket(ServerWebSocket ws,  JsonObject message){
    Promise<Void> written = Promise.promise();

    ws.writeTextMessage(message.toString(), written);

    return written.future();
  }

  private Future<Void> pingAllWebsockets(){
    List<Future> written =
    topicWebsocketMap.get().values()
            .stream()
            .flatMap(Collection::stream)
            .map(Pair::getLeft)
            .map(ws -> {
              Promise<Void> promise = Promise.promise();
              ws.writePing(Buffer.buffer(), promise);
              return (Future) promise.future();
            }).collect(Collectors.toList());

    return CompositeFuture.all(written)
            .onSuccess(aVoid -> LOGGER.debug(String.format("Pinged %s Websockets", written.size())))
            .flatMap(aVoid -> Future.succeededFuture());
  }

  @Override
  public void close(Promise<Void> closed) {
    if(this.findNewTopicsJobId != null){
      this.vertx.cancelTimer(findNewTopicsJobId);
    }
    if(this.pingAllWebsocketJobId != null){
      this.vertx.cancelTimer(pingAllWebsocketJobId);
    }

    closed.complete();
  }
}
