package io.piveau.DataStore.Connector.model.httpMessage;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

/**
 * Base HTTP Method with statusCode and message field.
 */
@DataObject(generateConverter = true, publicConverter = false)
public class HttpMessage {

  public Integer statusCode;
  public String message;

  public HttpMessage() {

  }

  // Needed for Data Object
  public HttpMessage(JsonObject json) {
    HttpMessageConverter.fromJson(json, this);
  }

  public JsonObject toJson() {
    return JsonObject.mapFrom(this);
  }

  public HttpMessage statusCode(Integer statusCode) {
    this.statusCode = statusCode;
    return this;
  }

  public HttpMessage message(String message) {
    this.message = message;
    return this;
  }
}
