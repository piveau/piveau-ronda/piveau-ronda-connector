package io.piveau.DataStore.Connector.model.datasetMessage;

import io.piveau.DataStore.Connector.model.httpMessage.HttpMessage;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Unified Message for DatasetLocations
 */
@DataObject(generateConverter = true, publicConverter = false)
public class DatasetMessage extends HttpMessage {

  public List<String> datasetLocations = new ArrayList<>();
  public List<String> fileNames = new ArrayList<>();
  public List<Long> fileSizes = new ArrayList<>();
  public Integer year;
  public Integer month;
  public Integer day;
  public Integer page;
  public Integer size;

  public DatasetMessage() {
  }

  // Needed for Data Object
  public DatasetMessage(JsonObject json) {
    DatasetMessageConverter.fromJson(json, this);
  }

  public DatasetMessage setLocationPrefix(String prefix) {
    this.datasetLocations =
      this.datasetLocations.stream()
        .map(dslocation -> prefix + dslocation)
        .collect(Collectors.toList());

    return this;
  }

  // Fluent Setter
  public DatasetMessage datasetLocations(List<String> datasetLocations) {
    this.datasetLocations = datasetLocations;
    return this;
  }

  public DatasetMessage fileNames(List<String> fileNames) {
    this.fileNames = fileNames;
    return this;
  }

  public DatasetMessage year(Integer year) {
    this.year = year;
    return this;
  }

  public DatasetMessage month(Integer month) {
    this.month = month;
    return this;
  }

  public DatasetMessage day(Integer day) {
    this.day = day;
    return this;
  }

  public DatasetMessage page(Integer page) {
    this.page = page;
    return this;
  }

  public DatasetMessage size(Integer size) {
    this.size = size;
    return this;
  }

  public DatasetMessage statusCode(Integer statusCode) {
    this.statusCode = statusCode;
    return this;
  }

  public DatasetMessage message(String message) {
    this.message = message;
    return this;
  }

  public DatasetMessage fileSizes(List<Long> fileSizes) {
    this.fileSizes = fileSizes;
    return this;
  }
}
