package io.piveau.DataStore.Connector.services.dataset;

import io.piveau.DataStore.Commons.schema.DataFormat;
import io.piveau.DataStore.Commons.schema.HdfsStructure;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.base.Key;
import io.piveau.DataStore.Commons.schema.base.Topic;
import io.piveau.DataStore.Connector.handler.DatasetHandler;
import io.piveau.DataStore.Connector.model.datasetMessage.DatasetMessage;
import io.piveau.DataStore.Connector.util.Constants;
import io.piveau.DataStore.Connector.util.DataStoreUtils;
import io.piveau.DataStore.Connector.util.HdfsUtils;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DatasetServiceImpl implements DatasetService{
  private static final Logger LOGGER = Logger.getLogger(DatasetService.class);
  HdfsUtils hdfsUtils;
  JsonObject config;
  List<String> advertisedTopics;

  /**
   * Service for managing Datasets and Datastream windows on the FileStore
   *
   * @param config       The Vertx Config provided by vertx ConfigRetriever
   * @param vertx        The Vertx Instance
   * @param readyHandler Handler that returns the instantiate object when it is ready
   */
  DatasetServiceImpl(JsonObject config, Vertx vertx, Handler<AsyncResult<DatasetService>> readyHandler) {
    Promise<HdfsUtils> hdfsReady = Promise.promise();
    this.hdfsUtils = new HdfsUtils(vertx, config, hdfsReady);
    this.config = config;

    this.advertisedTopics = config
            .getJsonArray(Constants.ADVERTISED_TOPICS, new JsonArray(KafkaTopics.openDataTopics().stream()
                    .map(t -> t.topicName)
                    .collect(Collectors.toList())
            ))
            .stream()
            .map(s -> (String) s)
            .collect(Collectors.toList());

    readyHandler.handle(Future.succeededFuture(this));
  }
  /**
   * Creates a simple HTML Dataset Uploadform to the Store for testing purpose
   *
   * @param readyHandler Handler that returns the finished uploadForm as html string
   * @return 'this' for fluent method chaining
   */
  public DatasetService getDatasetForm(Handler<AsyncResult<String>> readyHandler) {
    String uploadForm =
      "<form action=\"/dataset\" method=\"post\" enctype=\"multipart/form-data\">\n" +
        "    <div>\n" +
        "        <label for=\"name\">Api Key</label>\n" +
        "        <input type=\"text\" name=\"Authorization\" />\n" +
        "    </div>\n" +
        "    <div>\n" +
        "        <label for=\"name\">Select a file:</label>\n" +
        "        <input type=\"file\" name=\"file\" />\n" +
        "    </div>\n" +
        "    <div class=\"button\">\n" +
        "        <button type=\"submit\">Send</button>\n" +
        "    </div>" +
        "</form>";

    // Get Uploaded Files
    hdfsUtils.getFileStatus(new Path(hdfsUtils.hdfsStructure.uploadDir))
      .map(uploadedFiles -> {
        // Create Html Link for each file
        StringBuilder dataSetList;
        dataSetList = new StringBuilder("Uploaded Datasets:<br/>");
        for (FileStatus datasetStatus : uploadedFiles) {
          String datasetPath = "/dataset/" + datasetStatus.getPath().getName();
          dataSetList
            .append("<a " + "href='").append(datasetPath).append("'").append(">")
            .append(datasetStatus.getPath().getName())
            .append("</a> <br/>");
        }

        return dataSetList.toString();
      })
      .map(datasetListHtml -> uploadForm + datasetListHtml)
      .onSuccess(result -> readyHandler.handle(Future.succeededFuture(result)))
      .onFailure(error -> readyHandler.handle(Future.succeededFuture(uploadForm)));

    return this;
  }


  /**
   * Provides paginated datasetIds to existing Datasets
   *
   * @param page         page number for pagination
   * @param pageSize     size of one Page
   * @param readyHandler Handler that returns the datasetIds in the DatasetMessages 'datasetLocation' field
   * @return 'this' for fluent method chaining
   */
  public DatasetService getDatasetLocations(int page, int pageSize, Handler<AsyncResult<DatasetMessage>> readyHandler) {
    DatasetMessage result = new DatasetMessage().statusCode(200).message("List of Datasets");
    result.page = page;
    result.size = pageSize;

    // Get all Uploaded Files
    hdfsUtils.getFileStatus(new Path(hdfsUtils.hdfsStructure.uploadDir))
      // Filter for Pagination
      .map(datasetList -> DataStoreUtils.getPage(datasetList, result.page, result.size))
      .map(datasetList -> datasetList.stream()
        .map(fileStatus -> fileStatus.getPath().getName()).collect(Collectors.toSet())
      )
      .onFailure(error -> {
        LOGGER.error("Could not get DatasetList");
        LOGGER.debug(error);
        result.statusCode(500).message("Internal Server Error");

        readyHandler.handle(Future.succeededFuture(result));
      })
      .onSuccess(locations -> {
        result.datasetLocations.addAll(locations);
        result.statusCode = locations.size() > 0 ? 200 : 404;
        result.message = locations.size() > 0 ? result.message : "No Datasets found";
        readyHandler.handle(Future.succeededFuture(result));

      });

    return this;
  }

  /**
   * Provides the MetaData of a Dataset as DatasetMessage
   *
   * @param datasetId    id of the dataset
   * @param readyHandler Handler that returns the Metadata of the Dataset
   * @return 'this' for fluent method chaining
   */
  public DatasetService getDatasetMetadata(String datasetId, Handler<AsyncResult<DatasetMessage>> readyHandler) {
    DatasetMessage result = new DatasetMessage().statusCode(200).message("Dataset Metadata");

    hdfsUtils.getFileStatus(new Path(datasetId))
      .map(fileStatuses -> fileStatuses.get(0))
      .onSuccess(fileStatus -> {
        result.fileNames.add(fileStatus.getPath().getName());
        result.fileSizes.add(fileStatus.getLen());
        readyHandler.handle(Future.succeededFuture(result));
      })
      .onFailure(error -> {
        result.statusCode = 404;
        result.message = "Dataset does not exist";
        readyHandler.handle(Future.succeededFuture(result));
      });

    return this;
  }

  /**
   * Uploads a Dataset from the local Filesystem to the FileStore
   *
   * @param filePath     path to the dataset in local FileSystem
   * @param fileName     name of the Dataset
   * @param readyHandler Handler that returns the status of the upload in the 'statusCode' and 'message' field
   * @return 'this' for fluent method chaining
   */
  public DatasetService uploadDataset(String filePath, String fileName, Handler<AsyncResult<DatasetMessage>> readyHandler) {
    DatasetMessage result = new DatasetMessage().statusCode(202).message("Dataset Created");
    hdfsUtils.uploadFile(filePath, fileName)
      .map(id -> {
        result.fileNames.add(fileName);
        result.datasetLocations.add(id);
        return id;
      })
      .onSuccess(aVoid -> {
        readyHandler.handle(Future.succeededFuture(result));
      })
      .onFailure(error -> {
        readyHandler.handle(Future.succeededFuture(new DatasetMessage()
          .statusCode(500)
          .message("Internal Server Error")
        ));
      });

    return this;
  }

  /**
   * Update a Dataset on the FileStore
   *
   * @param datasetId    id of the  updating Dataset
   * @param filePath     path to the  new Dataset file in local FileSystem
   * @param fileName     name of the Dataset
   * @param readyHandler Handler that returns the status of the upload in the 'statusCode' and 'message' field
   * @return 'this' for fluent method chaining
   */
  public DatasetService replaceDataset(String datasetId, String filePath, String fileName, Handler<AsyncResult<DatasetMessage>> readyHandler) {
    DatasetMessage result = new DatasetMessage().statusCode(200).message("Dataset Updated");

    hdfsUtils.deleteFile(new Path(datasetId))
      .map(oldDatasetDeleted -> {

        if (!oldDatasetDeleted) {
          result.statusCode = 201;
          result.message = "Dataset created";
        }

        return oldDatasetDeleted;
      })
      .flatMap(deleted -> hdfsUtils.uploadFile(filePath, fileName, datasetId))
      .map(id -> {
        result.fileNames.add(fileName);
        result.datasetLocations.add(id);
        return id;
      })
      .onSuccess(success -> readyHandler.handle(Future.succeededFuture(result)))
      .onFailure(error -> readyHandler.handle(Future.succeededFuture(new DatasetMessage()
        .statusCode(500)
        .message("Internal Server Error"))));

    return this;
  }

  /**
   * Delete Dataset from the FileStore
   *
   * @param datasetId    id of the deleting Dataset
   * @param readyHandler Handler that returns the status of the deletion in the 'statusCode' and 'message' field
   * @return 'this' for fluent method chaining
   */
  public DatasetService deleteDataset(String datasetId, Handler<AsyncResult<DatasetMessage>> readyHandler) {
    DatasetMessage result = new DatasetMessage().statusCode(200).message("Dataset deleted");
    result.datasetLocations.add(datasetId);

    hdfsUtils.deleteFile(new Path(datasetId))
      .onFailure(error -> {
        result.statusCode = 500;
        result.message = "Internal Server Error";

        readyHandler.handle(Future.succeededFuture(result));
      })
      .onSuccess(fileDeleted -> {
        if (!fileDeleted) {
          result.statusCode = 404;
          result.message = "Dataset does not exist";
        }
        readyHandler.handle(Future.succeededFuture(result));
      });

    return this;
  }


  /**
   * Provides DataStreamIds to existing DataStreams
   *
   * @param page         page number for pagination
   * @param pageSize     size of one Page
   * @param readyHandler Handler that returns the DataStreamIds in the DatasetMessages 'datasetLocation' field
   * @return 'this' for fluent method chaining
   */
  public DatasetService getDataStreamList(int page, int pageSize, Handler<AsyncResult<DatasetMessage>> readyHandler) {
    DatasetMessage result = new DatasetMessage().statusCode(200).message("List of Datastreams")
      .page(page).size(pageSize);

    hdfsUtils.getFileStatus(new Path(hdfsUtils.hdfsStructure.opendataDir), HdfsUtils.noSparkInternalFiles)// Get all Datastream
      .map(rawhdfsFiles -> rawhdfsFiles.stream().map(fileStatus -> fileStatus.getPath().getName())) // Parse Results
      .map(rawDatastreamNames -> rawDatastreamNames.map(topic -> topic.substring(topic.lastIndexOf("=") + 1))) // Parse Results
      .map(topicStream -> topicStream.filter(topic -> this.advertisedTopics.contains(topic)))
      .map(parsedDataStreamLinks -> parsedDataStreamLinks.collect(Collectors.toList()))
      .map(rawhdfsFiles -> DataStoreUtils.getPage(rawhdfsFiles, page, pageSize)) // Filter Results



      .onFailure(error -> readyHandler.handle(Future.succeededFuture(
        new DatasetMessage()
          .statusCode(500).message("Internal Server Error")))
      )
      .onSuccess(topicLinks -> {
        result.datasetLocations.addAll(topicLinks);
        result.statusCode = topicLinks.size() > 0 ? 200 : 404;
        result.message = topicLinks.size() > 0 ? result.message : "No Datastreams found";
        readyHandler.handle(Future.succeededFuture(result));
      });

    return this;
  }


  /**
   * Provides Metadata such as File locations and FileSizes of a daily DataStream data in DataSetMessage
   *
   * @param dataStreamId id of the Datastream
   * @param year         year of data
   * @param month        month of data
   * @param day          of data
   * @param readyHandler Handler that returns the Metadata for the DataStream interval
   * @return 'this' for fluent method chaining
   */
  public DatasetService getMetadataDataStreamDay(String dataStreamId, int year, int month, int day, DataFormat format, Handler<AsyncResult<DatasetMessage>> readyHandler) {
    Topic topic =  KafkaTopics.valueOf(dataStreamId);
    if(topic == null){
      readyHandler.handle(Future.succeededFuture(new DatasetMessage().statusCode(404).message("Datastream does not exist")));
    }else {
      Key<?> key = topic.keys.iterator().next();


      Path topicPath = new Path(hdfsUtils.hdfsStructure.topicDirectory(topic, key, format));
      Path topicWindowPath = new Path(hdfsUtils.hdfsStructure.topicDirectory(topic, key, year, month, day, format));
      String filename = hdfsUtils.hdfsStructure.topicFileName(topic, year, month, day, format);

      getDataStreamMetadata(topicPath, topicWindowPath, filename)
        .map(datasetMessage -> datasetMessage.year(year).month(month).day(day))
        .onComplete(readyHandler);
    }
    return this;
  }


  /**
   * Provides Metadata of a monthly DataStream data in a DataSetMessage
   *
   * @param dataStreamId id of the Datastream
   * @param year         year of data
   * @param month        month of data
   * @param readyHandler Handler that returns the Metadata for the DataStream interval
   * @return 'this' for fluent method chaining
   */
  public DatasetService getMetadataDataStreamMonth(String dataStreamId, int year, int month, DataFormat format, Handler<AsyncResult<DatasetMessage>> readyHandler) {
    Topic topic =  KafkaTopics.valueOf(dataStreamId);

    if(topic == null){
      readyHandler.handle(Future.succeededFuture(new DatasetMessage().statusCode(404).message("Datastream does not exist")));
    }else {
      Key<?> key = topic.keys.iterator().next();
      Path topicPath = new Path(hdfsUtils.hdfsStructure.topicDirectory(topic, key, format));
      Path topicWindowPath = new Path(hdfsUtils.hdfsStructure.topicDirectory(topic, key, year, month, format));
      String filename = hdfsUtils.hdfsStructure.topicFileName(topic, year, month, format);

      getDataStreamMetadata(topicPath, topicWindowPath, filename)
        .map(datasetMessage -> datasetMessage.year(year).month(month))
        .onComplete(readyHandler);
    }
    return this;
  }

  /**
   * Provides Metadata of a DataStream Window in a DataSetMessage
   *
   * @param topicPath       Path to Topic on FileStore
   * @param topicWindowPath Path to the Topic on FileStore including the Timeframe
   * @param fileName        name of the created local downloaded File
   * @return DatasetMessage with Files
   */
  private Future<DatasetMessage> getDataStreamMetadata(Path topicPath, Path topicWindowPath, String fileName) {
    Promise<DatasetMessage> resultPromise = Promise.promise();
    DatasetMessage message = new DatasetMessage().statusCode(200).message("DataStream Files found");
    hdfsUtils.pathExists(topicPath)// Topic Exists
      .flatMap(topicExists -> {
        if (!topicExists) {
          message.statusCode = 404;
          message.message = "DataStream does not exist";
          return Future.failedFuture("DataStream does not exist");
        }

        return Future.succeededFuture();
      })
      .flatMap(aVoid -> hdfsUtils.pathExists(topicWindowPath)) // Time Period exists
      .flatMap(pathExists -> {
        if (!pathExists) {
          message.statusCode = 404;
          message.message = "No Data found for this time period";
          return Future.failedFuture("No Data found for this time period");
        }

        return Future.succeededFuture();
      })
      .flatMap(aVoid -> hdfsUtils.listFiles(topicWindowPath, true)) // Get FileStatus
      .map(fileStatuses -> fileStatuses.stream().filter(FileStatus::isFile).collect(Collectors.toList())) // Filter for Files
      .map(fileStatuses -> {
        fileStatuses.sort((fs1, fs2) -> fs1.getModificationTime() > fs2.getModificationTime() ? 1 : -1);
        return fileStatuses;
      }) // Sort for ModificationTime
      .onSuccess(fileStatuses -> {
        fileStatuses.forEach(fileStatus -> {
          message.fileSizes.add(fileStatus.getLen());
          message.datasetLocations.add(fileStatus.getPath().toString());
        });
        message.fileNames.add(fileName);

        resultPromise.complete(message);
      })
      .onFailure(error -> {
        if (message.statusCode == 200) {
          message.statusCode = 500;
          message.message = "Internal Server Error";
        }

        resultPromise.complete(message);
      });

    return resultPromise.future();
  }


  @Override
  public void close(Promise<Void> closed) {
    Promise<Void> hdfsClosed = Promise.promise();
    if(this.hdfsUtils != null) {
      this.hdfsUtils.close(closed);
    }else{
      closed.complete();
    }
  }
}
