package io.piveau.DataStore.Connector.services.dataset;

import io.piveau.DataStore.Commons.schema.DataFormat;
import io.piveau.DataStore.Connector.model.datasetMessage.DatasetMessage;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Closeable;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

@VertxGen
public interface DatasetService extends Closeable {

  static DatasetService create(JsonObject config, Vertx vertx, Handler<AsyncResult<DatasetService>> readyHandler) {
    return new DatasetServiceImpl(config, vertx, readyHandler);
  }

  @Fluent
  DatasetService getDatasetForm(Handler<AsyncResult<String>> readyHandler);

  @Fluent
  DatasetService getDatasetLocations(int page, int pageSize, Handler<AsyncResult<DatasetMessage>> readyHandler);

  @Fluent
  DatasetService getDatasetMetadata(String datasetId, Handler<AsyncResult<DatasetMessage>> readyHandler);

  @Fluent
  DatasetService uploadDataset(String filePath, String fileName, Handler<AsyncResult<DatasetMessage>> readyHandler);

  @Fluent
  DatasetService replaceDataset(String datasetId, String filePath, String fileName, Handler<AsyncResult<DatasetMessage>> readyHandler);

  @Fluent
  DatasetService deleteDataset(String datasetId, Handler<AsyncResult<DatasetMessage>> readyHandler);

  @Fluent
  DatasetService getDataStreamList(int page, int pageSize, Handler<AsyncResult<DatasetMessage>> readyHandler);

  @Fluent
  DatasetService getMetadataDataStreamMonth(String dataStreamId, int year, int month, DataFormat format, Handler<AsyncResult<DatasetMessage>> readyHandler);

  @Fluent
  DatasetService getMetadataDataStreamDay(String dataStreamId, int year, int month, int day, DataFormat format, Handler<AsyncResult<DatasetMessage>> readyHandler);
}
