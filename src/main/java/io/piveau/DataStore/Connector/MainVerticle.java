package io.piveau.DataStore.Connector;

import io.piveau.DataStore.Connector.handler.ApiKeyHandler;
import io.piveau.DataStore.Connector.handler.DatasetHandler;
import io.piveau.DataStore.Connector.handler.WebsocketHandler;
import io.piveau.DataStore.Connector.util.ConfigRetrieverOptionsProvider;
import io.piveau.DataStore.Connector.util.Constants;
import io.piveau.DataStore.Connector.util.DataStoreUtils;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.*;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.authentication.AuthenticationProvider;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.AuthenticationHandler;
import io.vertx.ext.web.handler.BasicAuthHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.openapi.RouterBuilder;
import io.vertx.ext.web.openapi.RouterBuilderOptions;
import org.apache.log4j.Logger;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {
  private static final Logger LOGGER = Logger.getLogger(MainVerticle.class);

  private HttpServer server;
  private ConfigRetriever configRetriever;
  private JsonObject config;


  private WebsocketHandler websocketHandler;
  private DatasetHandler datasetHandler;
  private ApiKeyHandler apiKeyHandler;


  /**
   * Starts the MainVerticle
   *
   * @param startPromise promise completes when Verticle has been started successfully
   * @throws Exception Potentially throws exception
   */
  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    // Instanciate
    configRetriever = ConfigRetriever.create(vertx, ConfigRetrieverOptionsProvider.options);

    Promise<JsonObject> configPromise = Promise.promise();
    configRetriever.getConfig(configPromise);

    configPromise
      .future()
      // Set Config
      .map(config -> this.config = config)
      // Start all Services
      .flatMap(aVoid -> CompositeFuture.all(startAuthServices(), startStreamingServices(), startBatchServices()))
      // Start the Server
      .flatMap(aVoid -> startServer())
            .onSuccess(success -> {
              startPromise.complete();
              LOGGER.info("Server started on Port:" + server.actualPort());
            })
            .onFailure(startPromise::fail);
  }

  /**
   * Stops the MainVerticle
   * @param stopPromise promise completes when Verticle has been stoped successfully
   */
  @Override
  public void stop(Promise<Void> stopPromise) {
    LOGGER.info("Shutting down...");
    if(this.configRetriever != null){
      this.configRetriever.close();
    }
    LOGGER.info("ConfigRetriever stopped");

    // Close Http Server
    Promise<Void> serverStopped = Promise.promise();
    if (this.server != null) {
      this.server.close(serverStopped);
    }else{
      serverStopped.complete();
    }
    LOGGER.info("HttpServer stopped");

    // Close Http Server
    Promise<Void> wsHandlerStopped = Promise.promise();
    if (this.websocketHandler != null){
      this.websocketHandler.close(wsHandlerStopped);
    }else{
      wsHandlerStopped.complete();
    }
    wsHandlerStopped.future().onSuccess(aVoid -> LOGGER.info("WebsocketHandler stopped"));
    // Close Http Server
    Promise<Void> dsHandlerStopped = Promise.promise();
    if (this.websocketHandler != null){
      this.datasetHandler.close(dsHandlerStopped);
    }else{
      dsHandlerStopped.complete();
    }
    dsHandlerStopped.future().onSuccess(aVoid -> LOGGER.info("DatasetHandler stopped"));


    CompositeFuture.all(
      wsHandlerStopped.future(),
      dsHandlerStopped.future(),
      serverStopped.future()
    )
      .onSuccess(success -> stopPromise.complete())
      .onFailure(stopPromise::fail);
  }

  /**
   * Starts the Http Server
   *
   * @return Future of the HttpServer instance
   */
  private Future<HttpServer> startServer() {
    Promise<HttpServer> promise = Promise.promise();

    LOGGER.info("Configuring Http Server");
    Promise<RouterBuilder> routerFactoryPromise = Promise.promise();

    RouterBuilder.create(this.vertx, "src/main/resources/webroot/openapi.json", routerFactoryPromise);
    routerFactoryPromise.
      future()
      .flatMap(this::createRouter)
      .flatMap(this::createServer)
      .onComplete(promise);

    return promise.future();
  }

  /**
   * Starts all Streaming Services for the Http Server
   *
   * @return Future succeeds if all Server could be started and fails otherwise
   */
  private Future<Void> startStreamingServices() {
    LOGGER.info("Starting Streaming Services");
    Promise<Void> promise = Promise.promise();
    websocketHandler = new WebsocketHandler(vertx, config);

    promise.complete();
    return promise.future();
  }

  /**
   * Starts all Batch Services for the Http Server
   *
   * @return Future succeeds if all Server could be started and fails otherwise
   */
  private Future<Void> startBatchServices() {
    LOGGER.info("Starting Batch Services");
    Promise<Void> result = Promise.promise();
    datasetHandler = new DatasetHandler(vertx, config);

    result.complete();
    return result.future();
  }


  /**
   * Starts all Batch Services for the Http Server
   *
   * @return Future succeeds if all Server could be started and fails otherwise
   */
  private Future<Void> startAuthServices() {
    LOGGER.info("Starting Batch Services");
    Promise<Void> result = Promise.promise();
    apiKeyHandler = new ApiKeyHandler(config.getString(Constants.API_KEY));

    result.complete();
    return result.future();
  }

  /**
   * Creates a Router instance out of the OpenApi3 Router Factory
   * by assigning the Handlers for OperationIds specified in the 'openapi.json'
   *
   * @param routerFactory The router Factory
   * @return the finished Router
   */
  private Future<Router> createRouter(RouterBuilder routerFactory) {
    Promise<Router> result = Promise.promise();
    LOGGER.info("Creating Router");

    // Create Options
    RouterBuilderOptions options = new RouterBuilderOptions()
      .setMountNotImplementedHandler(true)
      .setRequireSecurityHandlers(true);

    routerFactory.setOptions(options);
    // Mount Security Handlers<
    routerFactory.securityHandler("ApiKeyHeader", new ApiKeyHandler(config.getString(Constants.API_KEY)));

    //Mount Handler
    routerFactory.operation("getDatasetForm").handler(datasetHandler::getDatasetForm);

    // Route: /dataset
    routerFactory.operation("getDatasetList").handler(datasetHandler::getDatasetList);
    routerFactory.operation("postDataset").handler(datasetHandler::postDataset);

    // Route /dataset/<id>
    routerFactory.operation("getDataset").handler(datasetHandler::getDataset);
    routerFactory.operation("putDataset").handler(datasetHandler::putDataset);
    routerFactory.operation("deleteDataset").handler(datasetHandler::deleteDataset);

    // Route /datastream
    routerFactory.operation("getDataStreamList").handler(datasetHandler::getDataStreamList);
    // Route /datastream/<id>
    routerFactory.operation("getDataStream").handler(datasetHandler::getDataStream);
    routerFactory.operation("headDataStream").handler(datasetHandler::headDataStream);

    Router router = routerFactory.createRouter();
    // Open Api
    router.route("/*").handler(StaticHandler.create());

    result.complete(router);
    return result.future();
  }

  /**
   * Creates Http Server. Assigns the Router and the WebSocket Handler.
   *
   * @param router The Router for the Http Server
   * @return Future of the Finished Http Server
   */
  private Future<HttpServer> createServer(Router router) {
    Promise<HttpServer> result = Promise.promise();

    String host = config.getString(Constants.SERVER_HOST);
    int port = config.getInteger(Constants.SERVER_PORT);

    LOGGER.info("Creating Server");
    server = vertx.createHttpServer(new HttpServerOptions().setPort(port).setHost(host));

    server
      .webSocketHandler(websocketHandler::handleWebsocket) // Bind Websocket
      .requestHandler(router) // Bind Router
      .listen(result);

    return result.future();
  }

  public static void main(String[] args) {
    String[] params = Arrays.copyOf(args, args.length + 1);
    params[params.length - 1] = MainVerticle.class.getName();
    Launcher.executeCommand("run", params);
  }

}


