# Extend vert.x image
FROM vertx/vertx4:4.0.2
ENV VERTICLE_NAME io.piveau.DataStore.Connector.MainVerticle
ENV VERTICLE_FILE target/Connector-1.0.0-fat.jar

# Set the location of the verticles
ENV VERTICLE_HOME /usr/verticles

# Copy your verticle to the container
COPY $VERTICLE_FILE $VERTICLE_HOME/
COPY . $VERTICLE_HOME/

EXPOSE 8080
EXPOSE 80
# Launch the verticle
WORKDIR $VERTICLE_HOME
ENTRYPOINT ["sh", "-c"]
CMD ["exec vertx run $VERTICLE_NAME -cp $VERTICLE_HOME/*"]
